package com.waycare.CSVParser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.waycare.CSVParser.Types.FILE_TYPE;

public class DataBase {

	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");

	private HashMap<String, APCSVBean> dataBase;
	private LinkedList<String> keyVector;

	private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	private DateTimeFormatter dtf_h = DateTimeFormatter
			.ofPattern("HH dd/MM/yyyy");

	public DataBase(String startDate, String endDate) {

		dataBase = new HashMap<String, APCSVBean>();
		keyVector = new LinkedList<String>();

		init(startDate, endDate);

	}

	private void init(String startDateStr, String endDateStr) {

		LocalDateTime startDate = LocalDateTime.from(LocalDate.parse(
				startDateStr, dtf).atStartOfDay());
		LocalDateTime endDate = LocalDateTime.from(LocalDate.parse(endDateStr,
				dtf).atStartOfDay());

		while (startDate.isBefore(endDate)) {

			APCSVBean newAccidentBean = new APCSVBean();
			String time = startDate.format(dtf_h);
			dataBase.put(time, newAccidentBean);
			keyVector.add(time);
			startDate = startDate.plusHours(1);

		}
		startDate = LocalDateTime.from(LocalDate.parse(startDateStr, dtf)
				.atStartOfDay());

	}

	public void createRoadSectionCSV(String accidentFile, String weatherFile, String specialDayFile, String lightConditionFile,
			String outputFile, String outputKeyVectorFile) {

		storeInDB(accidentFile, FILE_TYPE.ACCIDENTS);
		storeInDB(weatherFile, FILE_TYPE.WEATHER);
		storeInDB(specialDayFile, FILE_TYPE.SPECIAL_DAYS);
		storeInDB(lightConditionFile, FILE_TYPE.LIGHT_CONDITION);

		// store dataBase in CSV file according to time
		storeInFile(outputFile,outputKeyVectorFile);

	}

	private void storeInFile(String dataBaseFile, String keyVectorFile) {

		try {

			CSVWriter writerDataBase = new CSVWriter(new FileWriter(dataBaseFile));
			CSVWriter writerKeyVector = new CSVWriter(new FileWriter(keyVectorFile));
			Iterator<String> itr = keyVector.iterator();
			while (itr.hasNext()) {
				String key = itr.next();
				writerDataBase.writeNext(dataBase.get(key).getCSVFormat(key));
				
				String[] keyCSV = new String[1];
				keyCSV[0] = key;
				writerKeyVector.writeNext(keyCSV);
			}

			writerDataBase.close();
			writerKeyVector.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void storeInDB(String inputFile, FILE_TYPE fileType) {

		try {

			CSVReader reader = new CSVReader(new FileReader(inputFile));
			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {

				switch (fileType) {
				case ACCIDENTS:
					storeAccident(nextLine);
					break;
				case WEATHER:
					storeWeather(nextLine);
					break;
				case SPECIAL_DAYS:
					storeSpecialDays(nextLine);
					break;
				case LIGHT_CONDITION:
					storeLightCondition(nextLine);
					break;
				}

			}

		} catch (FileNotFoundException e) {

			e.printStackTrace();
			System.exit(1);

		} catch (IOException e) {

			e.printStackTrace();
			System.exit(1);

		}
	}

	private void storeAccident(String[] line) {
		
		dataBase.get(line[0]).addAccident();

	}

	private void storeWeather(String[] line) {

		dataBase.get(line[0]).addWeather(line);

	}

	private void storeSpecialDays(String[] line) {
		
		dataBase.get(line[0]).setSpecialDay();
		
	}
	
	private void storeLightCondition(String[] line){
		
		if (line[1].equals("1")){
			dataBase.get(line[0]).setSunRise();
		}
		if (line[2].equals("1")){
			dataBase.get(line[0]).setSunSet();
		}
		if (line[3].equals("1")){
			dataBase.get(line[0]).setLight();
		}
		if (line[4].equals("1")){
			dataBase.get(line[0]).setDark();
		}
		
	}
	
	
	public void printStatistics() {

		Statistics statTrue = new Statistics();
		Statistics statFalse = new Statistics();

		Iterator<String> itr = keyVector.iterator();
		while (itr.hasNext()) {
			String key = itr.next();
			APCSVBean nextBean = dataBase.get(key);

			if (nextBean.numberOfAccidents > 0) {

				String[] line = nextBean.getCSVFormat(key);
				boolean accidentWithCondition = false;
				for (int i = 9; i < line.length-4; i++) {
					if (line[i].equals("1")) {
						
						statTrue.add(statTrue.conditions[i-9], Integer.valueOf(line[1]));
						accidentWithCondition = true;
					}
				}
				if (accidentWithCondition == false){
					statTrue.add("clear", Integer.valueOf(line[1]));
				}

			} else {
				
				String[] line = nextBean.getCSVFormat(key);
				boolean nonAccidentWithCondition = false;
				for (int i = 9; i < line.length-4; i++) {
					if (line[i].equals("1")) {
						
						statFalse.add(statFalse.conditions[i-9], Integer.valueOf("1"));
						nonAccidentWithCondition = true;
					}
				}
				if (nonAccidentWithCondition == false){
					statFalse.add("clear", Integer.valueOf("1"));
				}
			}
		}
		LOGGER.info("Accidents beans:");
		statTrue.print();
		LOGGER.info("----------------------------------------");
		LOGGER.info("NON Accidents beans:");
		statFalse.print();
	}

	private class Statistics {

		HashMap<String, Integer> stat = new HashMap<String, Integer>();

		public String conditions[] = { "fog", "ice fog", "haze", "lt rain",
				"mod rain", "hvy rain", "lt frz rain", "mod frz rain",
				"hvy frz rain", "lt snow", "mod snow", "hvy snow",
				"blowing snow", "lt ice", "mod ice", "hvy ice",
				"lt ice pellets", "mod ice pellets", "hvy ice pellets",
				"lt drizzle", "lt frz drizzle", "thunder", "mod thunder shwr",
				"squalls", "lt hail", "mod hail", "hvy hail" };

		public Statistics() {
			stat.put("fog", 0);
			stat.put("ice fog", 0);
			stat.put("haze", 0);
			stat.put("lt rain", 0);
			stat.put("mod rain", 0);
			stat.put("hvy rain", 0);
			stat.put("lt frz rain", 0);
			stat.put("mod frz rain", 0);
			stat.put("hvy frz rain", 0);
			stat.put("lt snow", 0);
			stat.put("mod snow", 0);
			stat.put("hvy snow", 0);
			stat.put("blowing snow", 0);
			stat.put("lt ice", 0);
			stat.put("mod ice", 0);
			stat.put("hvy ice", 0);
			stat.put("lt ice pellets", 0);
			stat.put("mod ice pellets", 0);
			stat.put("hvy ice pellets", 0);
			stat.put("lt drizzle", 0);
			stat.put("lt frz drizzle", 0);
			stat.put("thunder", 0);
			stat.put("mod thunder shwr", 0);
			stat.put("squalls", 0);
			stat.put("lt hail", 0);
			stat.put("mod hail", 0);
			stat.put("hvy hail", 0);
			stat.put("clear", 0);
		}

		public void add(String condition, int num) {
			stat.put(condition, stat.get(condition) + num);
		}
		
		public void print(){
			for (int i = 0; i < conditions.length; i++){
				LOGGER.info(conditions[i] + " " + stat.get(conditions[i]));
			}
			LOGGER.info("clear " + " " + stat.get("clear"));
		}

	}
}
