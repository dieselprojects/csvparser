package com.waycare.CSVParser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class AccidentsParser {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");

	private HashMap<String, LinkedList<AccidentBean>> sortDB[];
	private LinkedList<String> keyVector[];

	private boolean useDirections;
	private final int numberOfDirections;
	private final double granule;

	public AccidentsParser(boolean useDirections, int numberOfDirections,
			double granule) {

		this.numberOfDirections = (useDirections) ? numberOfDirections : 1;

		this.sortDB = new HashMap[this.numberOfDirections];
		this.keyVector = new LinkedList[this.numberOfDirections];

		for (int i = 0; i < this.numberOfDirections; i++) {
			sortDB[i] = new HashMap<String, LinkedList<AccidentBean>>();
			keyVector[i] = new LinkedList<String>();
		}

		this.useDirections = useDirections;
		this.granule = granule;

	}

	public void sort(String accidentFileLocation, String sortedFileLocation,
			boolean useCustomRoadSelection, String roadSection,
			int roadDirection) {

		LinkedList<AccidentBean> accidents[] = new LinkedList[numberOfDirections];
		for (int i = 0; i < numberOfDirections; i++) {
			accidents[i] = new LinkedList<AccidentBean>();
		}

		// load all accidents from accident file for both direction
		try {

			int loadIteration = 0;
			int dropCounter = 0;

			CSVReader reader = new CSVReader(new FileReader(
					accidentFileLocation));
			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {

				AccidentBean nextAccident;
				if ((nextAccident = createAccidentBean(loadIteration++,
						nextLine)) != null)

					// TODO: currently only North/South (change to all
					// direction according to road)
					if (useDirections) {
						switch (nextAccident.getDirection()) {
						case "N":
							accidents[0].add(nextAccident);
							break;
						case "S":
							accidents[1].add(nextAccident);
							break;
						default:
							break;
						}
					} else {
						accidents[0].add(nextAccident);
					}

				else {
					dropCounter++;
				}
			}

			reader.close();

		} catch (FileNotFoundException e) {

			e.printStackTrace();
			System.exit(1);

		} catch (IOException e) {

			e.printStackTrace();
			System.exit(1);

		}

		if (useDirections) {
			LOGGER.info("North counter " + accidents[0].size());
			LOGGER.info("South counter " + accidents[1].size());
		} else {
			LOGGER.info("number of accidents " + accidents[0].size());
		}
		// init sortedDB's according to max distance:
		// sort according to distance and store statistics
		for (int i = 0; i < numberOfDirections; i++) {

			init(accidents[i], i);
			sortAccordingToDistance(accidents[i], i);

		}

		// get section with max accidents:
		RoadSection bestRoadSection = getRoadSection(useCustomRoadSelection,
				roadSection, roadDirection);
		bestRoadSection.print();

		// store clean & sorted accident file
		store(bestRoadSection, sortedFileLocation);

	}

	public void store(RoadSection roadSection, String sortedFileLocation) {

		try {
			CSVWriter writer = new CSVWriter(
					new FileWriter(sortedFileLocation), ',');

			for (AccidentBean nextAccident : roadSection.getAccidents()) {

				writer.writeNext(nextAccident.getAccidentCSV());

			}

			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

	private void sortAccordingToDistance(LinkedList<AccidentBean> accidents,
			int direction) {

		HashMap<String, LinkedList<AccidentBean>> sortedDB = new HashMap<String, LinkedList<AccidentBean>>();

		for (AccidentBean accident : accidents) {

			String key = String
					.format("%.1f",
							(accident.getMilepost() - accident.getMilepost()
									% granule));
			sortDB[direction].get(key).add(accident);

		}

		for (String key : keyVector[direction]) {
			LOGGER.trace("key/number of accidents : [" + key + ","
					+ sortDB[direction].get(key).size() + "]");
		}
	}

	private AccidentBean createAccidentBean(int iteration, String[] line) {

		String route = line[19];
		String direction = line[20];
		String date = line[3];
		String time = line[5];

		if (time.equals("")) {
			LOGGER.trace("Accident time doesn't exist on line " + iteration
					+ " , dropping accident");
			return null;
		}

		if (time.length() != 4) {
			LOGGER.trace("Accident time not complete on line " + iteration
					+ " , trying to fix");

			if (time.length() == 2) {
				LOGGER.trace("assuming field contain hour only, fixing");
				time += "00";
			} else {
				LOGGER.trace("couldn't fix line, dropping accident");
				return null;
			}
		}

		double milePost;
		if (!line[24].equals("")) {

			milePost = Double.valueOf(line[24]);
			// TODO: currently non value for direction is South, check for
			// correctness
			if (direction.equals("")) {
				direction = "S";
			}

			return new AccidentBean(route, direction, date, time, milePost,
					false);

		} else {

			LOGGER.trace("Accident location doesn't exist on line " + iteration
					+ " , dropping accident");

		}
		return null;

	}

	private void init(LinkedList<AccidentBean> accidents, int directionNumber) {

		double maxDistance = 0.0;
		for (AccidentBean accident : accidents) {
			maxDistance = (accident.getMilepost() > maxDistance) ? accident
					.getMilepost() : maxDistance;
		}

		LOGGER.trace("Creating sorting beans according to distance: ");
		for (double distance = 0.0; distance < maxDistance; distance += granule) {
			keyVector[directionNumber].add(String.format("%.1f", distance));
			sortDB[directionNumber].put(String.format("%.1f", distance),
					new LinkedList<AccidentBean>());
			LOGGER.trace(distance + " mile");
		}
	}

	private RoadSection getRoadSection(boolean useCustomRoadSelection,
			String roadSection, int roadDirection) {

		if (useCustomRoadSelection) {

			return new RoadSection(roadSection, (roadDirection == 0) ? "N"
					: "S", sortDB[roadDirection].get(roadSection));

		} else {

			int maxAccidentsInSection = 0;
			String bestSectionMilePost = "";
			int bestSectionDirection = 0;

			for (int i = 0; i < numberOfDirections; i++) {

				for (String key : keyVector[i]) {

					if (sortDB[i].get(key).size() > maxAccidentsInSection) {
						maxAccidentsInSection = sortDB[i].get(key).size();
						bestSectionMilePost = key;
						bestSectionDirection = i;
					}

				}

			}
			return new RoadSection(bestSectionMilePost,
					(bestSectionDirection == 0) ? "N" : "S",
					sortDB[bestSectionDirection].get(bestSectionMilePost));
		}

	}

	// private class for return best section function
	private class RoadSection {

		private String milePost;
		private String direction;
		private LinkedList<AccidentBean> accidents;

		public RoadSection(String milePost, String direction,
				LinkedList<AccidentBean> accidents) {
			super();
			this.milePost = milePost;
			this.direction = direction;
			this.accidents = accidents;
		}

		public String getMilePost() {
			return milePost;
		}

		public void setMilePost(String milePost) {
			this.milePost = milePost;
		}

		public String getDirection() {
			return direction;
		}

		public void setDirection(String direction) {
			this.direction = direction;
		}

		public LinkedList<AccidentBean> getAccidents() {
			return accidents;
		}

		public void setAccidents(LinkedList<AccidentBean> accidents) {
			this.accidents = accidents;
		}

		public void print() {
			LOGGER.info("Best read section " + milePost + " Direction "
					+ direction + " with " + accidents.size() + " Accidents");
		}

	}
}
