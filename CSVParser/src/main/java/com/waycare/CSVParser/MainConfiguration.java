package com.waycare.CSVParser;

import com.waycare.CSVParser.Types.FILES_HANDLE;
import com.waycare.CSVParser.Types.ROUTES;

public class MainConfiguration {
	
	private static MainConfiguration instance = null;

	protected MainConfiguration() {
		// Exists only to defeat instantiation.
	}

	public static MainConfiguration getInstance() {
		if (instance == null) {
			instance = new MainConfiguration();
		}
		return instance;
	}

	public FILES_HANDLE filesHandle = FILES_HANDLE.CLEAN;
	
	public String startDate = "01/01/2005";
	public String endDate = "01/01/2013";

	// TODO: implement useAllRoutes
	public boolean useAllRoutes = false;
	public ROUTES useRoutes = ROUTES.GSP;
	
	// File Locations per road:
	// Garden state parkway:
	String[] gspInputFileLocations = new String[]{
		"Res/Accidents/garden_state_accidents.csv",
		"Res/Weather/NewarkNewarkInternationalAirport_40.68250_-74.16944/weather.csv",
		//"Res/Weather/weather_conditions_gsp_140.csv",
		"Res/SpecialDays/special_days_nj.csv",
		"Res/LightCondition/light_condition.csv"
	};
	String gspOutputFileLocation = "Res/gsp_data.csv";
	String gspKeyVectorOutputFileLocation = "Res/gsp_key_vector.csv";
	
	// US 9:
	// current weather station located near milepost 40 is atlantic city
	String[] us9InputFileLocations = new String[]{
		"Res/Accidents/us_9_accidents.csv",
		"Res/Weather/AtlanticCityAtlanticCityInternationalAirport_39.45202_-74.56699/weather.csv",
		//"Res/Weather/NewarkNewarkInternationalAirport_40.68250_-74.16944/weather.csv",
		//"Res/Weather/SomervilleSomersetAirport_40.62405_-74.66898/weather.csv",
		//"Res/Weather/TeterboroTeterboroAirport_40.85889_-74.05667/weather.csv",
		//"Res/Weather/CaldwellEssexCountyAirport_40.87639_-74.28306/weather.csv",
		//"Res/Weather/AtlanticCityAtlanticCityInternationalAirport_39.45202_-74.56699/weather.csv",
		//"Res/Weather/SouthJerseyRegionalAirport_39.94076_-74.84071/weather.csv",
		"Res/SpecialDays/special_days_nj.csv",
		"Res/LightCondition/light_condition.csv"
	};
	
	String us9OutputFileLocation = "Res/us9_data.csv";
	String us9KeyVectorOutputFileLocation = "Res/us9_40m_key_vector.csv";

	// US 206:
	String[] us206InputFileLocations = new String[]{
		"Res/Accidents/us_206_accidents.csv",
		//"Res/Weather/SomervilleSomersetAirport_40.62405_-74.66898/weather.csv",
		"Res/Weather/TeterboroTeterboroAirport_40.85889_-74.05667/weather.csv",
		"Res/SpecialDays/special_days_nj.csv",
		"Res/LightCondition/light_condition.csv"
	};
	
	String us206OutputFileLocation = "Res/us_206_data.csv";
	String us206KeyVectorOutputFileLocation = "Res/us_206_key_vector.csv";
	
	
	// I 80:
	String[] i80InputFileLocations = new String[]{
		"Res/Accidents/i_80_accidents.csv",
		"Res/Weather/CaldwellEssexCountyAirport_40.87639_-74.28306/weather.csv",
		"Res/SpecialDays/special_days_nj.csv",
		"Res/LightCondition/light_condition.csv"
	};
	
	String i80OutputFileLocation = "Res/i_80_data.csv";
	String i80KeyVectorOutputFileLocation = "Res/i_80_key_vector.csv";
	
	// US-1:
	String[] us1InputFileLocations = new String[]{
		"Res/Accidents/us1_accidents.csv",
		//"Res/Weather/NewarkNewarkInternationalAirport_40.68250_-74.16944/weather.csv",
		//"Res/Weather/SomervilleSomersetAirport_40.62405_-74.66898/weather.csv",
		//"Res/Weather/TeterboroTeterboroAirport_40.85889_-74.05667/weather.csv",
		//"Res/Weather/CaldwellEssexCountyAirport_40.87639_-74.28306/weather.csv",
		//"Res/Weather/AtlanticCityAtlanticCityInternationalAirport_39.45202_-74.56699/weather.csv",
		"Res/Weather/SouthJerseyRegionalAirport_39.94076_-74.84071/weather.csv",
		"Res/SpecialDays/special_days_nj.csv",
		"Res/LightCondition/light_condition.csv"
	};
	
	String us1OutputFileLocation = "Res/us_1_data.csv";
	String us1KeyVectorOutputFileLocation = "Res/us_1_key_vector.csv";
	
	// NJ-17:
	String[] nj17InputFileLocations = new String[]{
		"Res/Accidents/nj_17_accidents.csv",
		"Res/Weather/TeterboroTeterboroAirport_40.85889_-74.05667/weather.csv",
		"Res/SpecialDays/special_days_nj.csv",
		"Res/LightCondition/light_condition.csv"
	};
	
	String nj17OutputFileLocation = "Res/nj_17_data.csv";
	String nj17KeyVectorOutputFileLocation = "Res/nj_17_key_vector.csv";
	
	
	
	// accidents related configuration
	public String cleanAccidentFileLocation = "Res/clean_accidents.csv";
	public String cleanAccidentSortedFileLocation = "Res/clean_sorted_accidents.csv";
	public double granule = 5.0;
	
	// weather related configuration:
	public String cleanWeatherFileLocation = "Res/clean_weather.csv";
	public String cleanConvertedWeatherFileLocation = "Res/clean_converted_weather.csv";

	// special days configuration:
	public String cleanSpecialDaysFileLocation = "Res/clean_special_days_nj.csv";
	public String cleanConvertedSpecialDaysFileLocation = "Res/clean_converted_special_days_nj.csv";

	// light condition:
	public String cleanLightConditionFileLocation = "Res/clean_light_condition.csv";
	public String cleanConvertedLightConditionFileLocation = "Res/clean_converted_light_condition.csv";
	
	public boolean useDirections = false;
	public int numberOfDirections = 1;
	
	// custom road section selector:
	boolean useCustomRoadSection = true;
	String customRoadSection = "145.0";
	int customRoadDirection = 0; // [0] - N, [1] - S
}
