package com.waycare.CSVParser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class SpecialDayTimeConvertor {
	
	DateTimeFormatter specialDay_dtf = DateTimeFormatter.ofPattern("yyyy MMM dd");
	DateTimeFormatter dtf_h = DateTimeFormatter.ofPattern("HH dd/MM/yyyy");
	
	public void convert(String inputFile, String outputFile){
		
		try {
			CSVReader reader = new CSVReader(new FileReader(inputFile));
			CSVWriter writer = new CSVWriter(new FileWriter(outputFile),',');
		
			String[] nextLine;
			while((nextLine = reader.readNext()) != null){
				LocalDate specialDayDate = LocalDate.parse(nextLine[0], specialDay_dtf);
				LocalDateTime specialDayDateTime = LocalDateTime.of(specialDayDate, LocalTime.MIN);
				String[] specialDayLine = new String[1];
				for (int i = 0; i < 24; i++){
					LocalDateTime plus = specialDayDateTime.plusHours(i);
					specialDayLine[0] = plus.format(dtf_h);
					writer.writeNext(specialDayLine);
				}
				
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
	}
}
