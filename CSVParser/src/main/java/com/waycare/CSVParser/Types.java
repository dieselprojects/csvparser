package com.waycare.CSVParser;

public class Types {

	public enum FILE_TYPE{
		ACCIDENTS,WEATHER,SPECIAL_DAYS,LIGHT_CONDITION
	}
	
	public enum FILES_HANDLE{
		KEEP,CLEAN
	}
	
	public enum ROUTES{
		GSP,US9,US206,I80,US1,NJ17
	}
}
