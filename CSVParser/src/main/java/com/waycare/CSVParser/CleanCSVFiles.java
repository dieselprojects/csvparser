package com.waycare.CSVParser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.waycare.CSVParser.Types.FILE_TYPE;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

/*
 * Description:
 * 
 * 	remove unneeded/corrupted accidents and weather samples according to dated and store in clean csv files
 * 	
 */
public class CleanCSVFiles {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");

	DateTimeFormatter configuration_dtf = DateTimeFormatter
			.ofPattern("dd/MM/yyyy");
	LocalDate startDate;
	LocalDate endDate;

	DateTimeFormatter accident_dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
	DateTimeFormatter weather_dtf = DateTimeFormatter.ofPattern("M-d-yyyy");
	DateTimeFormatter specialDays_dtf = DateTimeFormatter
			.ofPattern("yyyy MMM dd");
	DateTimeFormatter lightCondition_dtf = DateTimeFormatter
			.ofPattern("HH dd/MM/yyyy");

	public CleanCSVFiles(String startDateStr, String endDateStr) {

		this.startDate = LocalDate.parse(startDateStr, configuration_dtf);
		this.endDate = LocalDate.parse(endDateStr, configuration_dtf);

	}

	public void clean(String inputFileLocation, String outputFileLocation,
			FILE_TYPE fileType) {

		try {

			CSVReader reader = new CSVReader(new FileReader(inputFileLocation));
			CSVWriter writer = new CSVWriter(
					new FileWriter(outputFileLocation), ',');

			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {

				boolean checkResult = false;
				switch (fileType) {
				case ACCIDENTS:
					checkResult = checkAccident(nextLine);
					break;
				case WEATHER:
					checkResult = checkWeather(nextLine);
					break;
				case SPECIAL_DAYS:
					checkResult = checkSpecialDay(nextLine);
					break;
				case LIGHT_CONDITION:
					checkResult = checkLightCondition(nextLine);
					break;
				default:
					break;
				}

				if (checkResult) {
					writer.writeNext(nextLine);
				}

			}

			reader.close();
			writer.close();

		} catch (FileNotFoundException e) {

			e.printStackTrace();
			System.exit(1);

		} catch (IOException e) {

			e.printStackTrace();
			System.exit(1);

		}

	}

	private boolean checkAccident(String[] line) {

		String route = line[19];
		String direction = line[20];
		String date = line[3];
		String time = line[5];

		// check time corruption
		if (time.equals("")) {
			return false;
		}

		if (time.length() != 4) {

			if (time.length() == 2) {
				time += "00";
			} else {
				return false;
			}
		}

		// check location corruption:
		if (line[24].equals("")) {
			return false;
		}

		// check if date inside time frame
		LocalDate accidentDate = LocalDate.parse(date, accident_dtf);

		return (accidentDate.isEqual(startDate) || accidentDate
				.isAfter(startDate) && accidentDate.isBefore(endDate));

	}

	private boolean checkWeather(String[] line) {

		// check date/time corruption:
		if (line[0].equals("")) {
			return false;
		}

		// check if date inside time frame
		String[] timeDate = line[0].split(" ");
		LocalDate weatherDate = LocalDate.parse(timeDate[0], weather_dtf);
		return (weatherDate.isEqual(startDate) || weatherDate
				.isAfter(startDate) && weatherDate.isBefore(endDate));

	}

	private boolean checkSpecialDay(String[] line) {

		LocalDate specialDayDate = LocalDate.parse(line[0], specialDays_dtf);
		return (specialDayDate.isEqual(startDate) || specialDayDate
				.isAfter(startDate) && specialDayDate.isBefore(endDate));

	}

	private boolean checkLightCondition(String[] line) {

		LocalDate lightConditionDate = LocalDate.parse(line[0],
				lightCondition_dtf);
		return (lightConditionDate.isEqual(startDate) || lightConditionDate
				.isAfter(startDate) && lightConditionDate.isBefore(endDate));

	}

}
