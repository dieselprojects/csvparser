package com.waycare.CSVParser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class LightConditionConvertor {
/*
 * Description:
 * 	Separate sun rise and sun set, 
 */
	
	DateTimeFormatter dtf_h = DateTimeFormatter.ofPattern("HH dd/MM/yyyy");
	
	public void convert(String inputFile, String outputFile){
		
		try {
			CSVReader reader = new CSVReader(new FileReader(inputFile));
			CSVWriter writer = new CSVWriter(new FileWriter(outputFile),',');
		
			String[] nextLine;
			while((nextLine = reader.readNext()) != null){
				
				String[] nextWriteLine = {nextLine[0],"0","0",nextLine[2],nextLine[3]};
				LocalDateTime lightConditionsDateTime = LocalDateTime.parse(nextLine[0], dtf_h);
				LocalDateTime lightConditionMidDay = LocalDateTime.of(lightConditionsDateTime.toLocalDate(), LocalTime.MIDNIGHT);
				
				if (nextLine[1].equals("1")){
					nextWriteLine[1] = ( lightConditionsDateTime.getHour() < 12 ) ? "1" : "0";
					nextWriteLine[2] = ( lightConditionsDateTime.getHour() > 12 ) ? "1" : "0";
				}
				writer.writeNext(nextWriteLine);
				
			}
			reader.close();
			writer.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
	}
	

}
