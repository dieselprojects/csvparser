package com.waycare.CSVParser;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class AccidentBean {

	DateTimeFormatter accident_dtf_h = DateTimeFormatter.ofPattern("HHmm MM/dd/yyyy");
	private DateTimeFormatter dtf_h = DateTimeFormatter
			.ofPattern("HH dd/MM/yyyy");
	
	
	
	private String route;
	private String direction;
	private String date;
	private String time;
	private double milepost;
	private boolean truckInvolve;
	
	// for clustering algorithm
	private int clusterNumber;

	public AccidentBean(){};

	public AccidentBean(String route, String direction, String date,
			String time, double milepost, boolean truckInvolve) {
		this.route = route;
		this.direction = direction;
		this.date = date;
		this.time = time;
		this.milepost = milepost;
		this.truckInvolve = truckInvolve;
	}
	
	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public double getMilepost() {
		return milepost;
	}

	public void setMilepost(double milepost) {
		this.milepost = milepost;
	}

	public int getClusterNumber() {
		return clusterNumber;
	}

	public void setClusterNumber(int clusterNumber) {
		this.clusterNumber = clusterNumber;
	}
	
	public String[] getAccidentCSV(){
		
		String key = time + " " + date;
		LocalDateTime accidentTime = LocalDateTime.parse(key, accident_dtf_h);
		// reduce one hour (between actual accident and police arrival)
		if (accidentTime.getMinute() < 30)
			accidentTime.minusHours(1);
		
		String[] csvFormat = new String[2];
		csvFormat[0] = accidentTime.format(dtf_h);
		csvFormat[1] = (truckInvolve) ? "1" : "0";;
		return csvFormat; 
		
	}
	
}
