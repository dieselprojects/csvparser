package com.waycare.CSVParser;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.waycare.CSVParser.Types.FILES_HANDLE;
import com.waycare.CSVParser.Types.FILE_TYPE;

/**
 * Description: Receives accidents csv file parse, divide according to milePost
 * and gives statistics Receives weather files according to weather station try
 * to match according to road milePost (weather station file contain road
 * section corresponding to road milePost), if match found create csv file with
 * time/date accidents and weather condition
 * 
 * Orginize both accidents and weather according to configuration time frame
 * 
 * TODO: currently only gsg with milePost 140 is supported, develop project for
 * more stations
 */
public class CSVParser {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");

	public static void main(String[] args) {

		if (cfg.useAllRoutes) {
			// TODO: implement this
			parseRoute(cfg.gspInputFileLocations, cfg.gspOutputFileLocation,
					cfg.gspKeyVectorOutputFileLocation);
			parseRoute(cfg.us9InputFileLocations, cfg.us9OutputFileLocation,
					cfg.us9KeyVectorOutputFileLocation);
			
		} else {
			switch (cfg.useRoutes) {
			case GSP:
				parseRoute(cfg.gspInputFileLocations,
						cfg.gspOutputFileLocation,
						cfg.gspKeyVectorOutputFileLocation);
				break;
			case US9:
				parseRoute(cfg.us9InputFileLocations,
						cfg.us9OutputFileLocation,
						cfg.us9KeyVectorOutputFileLocation);
				break;
			case US206:
				parseRoute(cfg.us206InputFileLocations,
						cfg.us206OutputFileLocation,
						cfg.us206KeyVectorOutputFileLocation);
				break;
			case I80:
				parseRoute(cfg.i80InputFileLocations,
						cfg.i80OutputFileLocation,
						cfg.i80KeyVectorOutputFileLocation);
				break;
			case US1:
				parseRoute(cfg.us1InputFileLocations,
						cfg.us1OutputFileLocation,
						cfg.us1KeyVectorOutputFileLocation);
				break;
			case NJ17:
				parseRoute(cfg.nj17InputFileLocations,
						cfg.nj17OutputFileLocation,
						cfg.nj17KeyVectorOutputFileLocation);
				break;
			default:
				break;
			}
		}

	}

	private static void parseRoute(String[] inputFiles, String outputFile,
			String keyVectorOutputFile) {

		LOGGER.info("Start parsing on " + inputFiles[0]);

		// main dataBase:
		// --------------
		CleanCSVFiles cleanCSVFiles = new CleanCSVFiles(cfg.startDate,
				cfg.endDate);

		// Accidents:
		// ----------
		cleanCSVFiles.clean(inputFiles[0], cfg.cleanAccidentFileLocation,
				FILE_TYPE.ACCIDENTS);
		AccidentsParser accidentsParser = new AccidentsParser(
				cfg.useDirections, cfg.numberOfDirections, cfg.granule);
		accidentsParser.sort(cfg.cleanAccidentFileLocation,
				cfg.cleanAccidentSortedFileLocation, cfg.useCustomRoadSection,
				cfg.customRoadSection, cfg.customRoadDirection);

		// Weather:
		// --------
		cleanCSVFiles.clean(inputFiles[1], cfg.cleanWeatherFileLocation,
				FILE_TYPE.WEATHER);
		WeatherTimeConvertor weatherTimeConvertor = new WeatherTimeConvertor();
		weatherTimeConvertor.convert(cfg.cleanWeatherFileLocation,
				cfg.cleanConvertedWeatherFileLocation);

		// Special days:
		// -------------
		cleanCSVFiles.clean(inputFiles[2], cfg.cleanSpecialDaysFileLocation,
				FILE_TYPE.SPECIAL_DAYS);
		SpecialDayTimeConvertor specialDayTimeConvertor = new SpecialDayTimeConvertor();
		specialDayTimeConvertor.convert(cfg.cleanSpecialDaysFileLocation,
				cfg.cleanConvertedSpecialDaysFileLocation);

		// Light condition:
		// ----------------
		cleanCSVFiles.clean(inputFiles[3], cfg.cleanLightConditionFileLocation,
				FILE_TYPE.LIGHT_CONDITION);
		LightConditionConvertor lightConditionConverter = new LightConditionConvertor();
		lightConditionConverter.convert(cfg.cleanLightConditionFileLocation,
				cfg.cleanConvertedLightConditionFileLocation);

		DataBase dataBase = new DataBase(cfg.startDate, cfg.endDate);
		dataBase.createRoadSectionCSV(cfg.cleanAccidentSortedFileLocation,
				cfg.cleanConvertedWeatherFileLocation,
				cfg.cleanConvertedSpecialDaysFileLocation,
				cfg.cleanConvertedLightConditionFileLocation, outputFile,
				keyVectorOutputFile);

		dataBase.printStatistics();

		if (cfg.filesHandle == FILES_HANDLE.CLEAN) {

			try {
				Files.delete(FileSystems.getDefault().getPath(
						cfg.cleanAccidentFileLocation));
				Files.delete(FileSystems.getDefault().getPath(
						cfg.cleanWeatherFileLocation));
				Files.delete(FileSystems.getDefault().getPath(
						cfg.cleanSpecialDaysFileLocation));
				Files.delete(FileSystems.getDefault().getPath(
						cfg.cleanLightConditionFileLocation));
				Files.delete(FileSystems.getDefault().getPath(
						cfg.cleanConvertedLightConditionFileLocation));
				Files.delete(FileSystems.getDefault().getPath(
						cfg.cleanConvertedSpecialDaysFileLocation));
				Files.delete(FileSystems.getDefault().getPath(
						cfg.cleanConvertedWeatherFileLocation));
				Files.delete(FileSystems.getDefault().getPath(
						cfg.cleanAccidentSortedFileLocation));
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}

		}
	}
}
