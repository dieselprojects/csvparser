package com.waycare.CSVParser;

public class APCSVBean {

	private static final int NUMBER_OF_PARAMETERS = 39;
	
	// output:
	public int numberOfAccidents;
	// special days:
	public String specialDay = "0";
	// weather:
	public double precipitation;
	public double visibility = 10.0;
	public double wind;
	public double windDirection;
	public double windGust;
	public double surfaceSnow = 0.0;
	public String fog = "0";
	public String ice_fog = "0";
	public String haze = "0";
	public String lt_rain = "0";
	public String mod_rain = "0";
	public String hvy_rain = "0";
	public String lt_frz_rain = "0";
	public String mod_frz_rain = "0";
	public String hvy_frz_rain = "0";
	public String lt_snow = "0";
	public String mod_snow = "0";
	public String hvy_snow = "0";
	public String blowing_snow = "0";
	public String lt_ice = "0";
	public String mod_ice = "0";
	public String hvy_ice = "0";
	public String lt_ice_pellets = "0";
	public String mod_ice_pellets = "0";
	public String hvy_ice_pellets = "0";
	public String lt_drizzle = "0";
	public String lt_frz_drizzle = "0";
	public String thunder = "0";
	public String mod_thunder_shwr = "0";
	public String squalls = "0";
	public String lt_hail = "0";
	public String mod_hail = "0";
	public String hvy_hail = "0";
	// light condition:
	public String sunRise = "0";
	public String sunSet = "0";
	public String light = "0";
	public String dark = "0";
	
	public void addAccident() {
		numberOfAccidents++;
	}
	
	public void setSpecialDay(){
		this.specialDay = "1";
	}

	public void addWeather(String[] weather) {

		String wind = (weather[1].equals("")) ? "0.0" : weather[1];
		String windDirection = (weather[3].equals("")) ? "0.0" : weather[3];
		String windGust = (weather[2].equals("")) ? "0.0" : weather[2];
		addWindCondition(Double.valueOf(wind), Double.valueOf(windDirection),
				Double.valueOf(windGust));

		String weatherConditions = weather[4];
		addWeatherConditions(weatherConditions);

		String visibility = (weather[5].equals("")) ? "10.0" : weather[5];
		addVisibility(Double.valueOf(visibility));

		String precipitation = (weather[6].equals("")) ? "0.0" : weather[6];
		addPrecipitation(Double.valueOf(precipitation));

	}

	public void addPrecipitation(double precipitationTick) {
		precipitation += precipitationTick;
	}

	public void addVisibility(double visibility) {
		if (visibility == 0.0) {

			this.visibility = visibility;

		} else {
			// store lowest visibility:
			this.visibility = (this.visibility > visibility) ? visibility
					: this.visibility;

		}
	}

	public void addWindCondition(double wind, double windDirection,
			double windGust) {
		if (this.wind == 0) {
			this.wind = wind;
			this.windDirection = windDirection;
			this.windGust = windGust;
		} else {
			// store strongest wind in 90* to 180*
			if ((windDirection > 90.0 && windDirection < 180.0)
					&& (wind > this.wind)) {
				this.wind = wind;
				this.windDirection = windDirection;
				if (windGust > this.windGust)
					this.windGust = windGust;

			}

		}
	}

	public void addSurfaceSnow(double surfaceSnow) {
		if (this.surfaceSnow < surfaceSnow) {
			this.surfaceSnow = surfaceSnow;
		}
	}

	public void addWeatherConditions(String weatherConditions) {

		String[] conditionArr = weatherConditions.split(";");

		for (int i = 0; i < conditionArr.length; i++) {

			// remove space at beginning
			if (i > 0)
				conditionArr[i] = conditionArr[i].replaceFirst(" ", "");

			switch (conditionArr[i]) {
			case "fog":
				fog = "1";
				break;
			case "ice fog":
				ice_fog = "1";
				break;
			case "haze":
				haze = "1";
				break;
			case "lt rain":
				lt_rain = "1";
				break;
			case "mod rain":
				mod_rain = "1";
				break;
			case "hvy rain":
				hvy_rain = "1";
				break;
			case "lt rain shwr":
				lt_rain = "1";
				break;
			case "mod rain shwr":
				mod_rain = "1";
				break;
			case "hvy rain shwr":
				hvy_rain = "1";
				break;
			case "lt frz rain":
				lt_frz_rain = "1";
				break;
			case "mod frz rain":
				mod_frz_rain = "1";
				break;
			case "hvy frz rain":
				hvy_frz_rain = "1";
				break;
			case "lt snow":
				lt_snow = "1";
				break;
			case "mod snow":
				mod_snow = "1";
				break;
			case "hvy snow":
				hvy_snow = "1";
				break;
			case "blowing snow":
				blowing_snow = "1";
				break;
			case "lt ice":
				lt_ice = "1";
				break;
			case "mod ice":
				mod_ice = "1";
				break;
			case "hvy ice":
				hvy_ice = "1";
				break;
			case "lt ice pellets":
				lt_ice_pellets = "1";
				break;
			case "mod ice pellets":
				mod_ice_pellets = "1";
				break;
			case "hvy ice pellets":
				hvy_ice_pellets = "1";
				break;
			case "lt drizzle":
				lt_drizzle = "1";
				break;
			case "lt frz drizzle":
				lt_frz_drizzle = "1";
				break;
			case "thunder":
				thunder = "1";
				break;
			case "mod thunder shwr":
				mod_thunder_shwr = "1";
				break;
			case "squalls":
				squalls = "1";
				break;
			case "lt hail":
				lt_hail = "1";
				break;
			case "mod hail":
				mod_hail = "1";
				break;
			case "hvy hail":
				hvy_hail = "1";
				break;

			}
		}
	}
	
	
	public void setSunRise(){
		this.sunRise = "1";
	}
	
	public void setSunSet(){
		this.sunSet = "1";
	}

	public void setLight(){
		this.light = "1";
	}

	public void setDark(){
		this.dark = "1";
	}

	public String[] getCSVFormat(String key) {

		String[] returnStrArr = new String[NUMBER_OF_PARAMETERS + 1];

		returnStrArr[0] 	= key;
		returnStrArr[1] 	= String.valueOf(numberOfAccidents);
		returnStrArr[2] 	= specialDay;
		returnStrArr[3] 	= String.valueOf(precipitation);
		returnStrArr[4] 	= String.valueOf(visibility);
		returnStrArr[5] 	= String.valueOf(wind);
		returnStrArr[6] 	= String.valueOf(windGust);
		returnStrArr[7] 	= String.valueOf(windDirection);
		returnStrArr[8] 	= String.valueOf(surfaceSnow);
		returnStrArr[9] 	= fog;
		returnStrArr[10]	= ice_fog;
		returnStrArr[11] 	= haze;
		returnStrArr[12] 	= lt_rain;
		returnStrArr[13] 	= mod_rain;
		returnStrArr[14] 	= hvy_rain;
		returnStrArr[15] 	= lt_frz_rain;
		returnStrArr[16] 	= mod_frz_rain;
		returnStrArr[17] 	= hvy_frz_rain;
		returnStrArr[18] 	= lt_snow;
		returnStrArr[19] 	= mod_snow;
		returnStrArr[20] 	= hvy_snow;
		returnStrArr[21] 	= blowing_snow;
		returnStrArr[22] 	= lt_ice;
		returnStrArr[23] 	= mod_ice;
		returnStrArr[24] 	= hvy_ice;
		returnStrArr[25] 	= lt_ice_pellets;
		returnStrArr[26] 	= mod_ice_pellets;
		returnStrArr[27] 	= hvy_ice_pellets;
		returnStrArr[28] 	= lt_drizzle;
		returnStrArr[29] 	= lt_frz_drizzle;
		returnStrArr[30] 	= thunder;
		returnStrArr[31] 	= mod_thunder_shwr;
		returnStrArr[32] 	= squalls;
		returnStrArr[33] 	= lt_hail;
		returnStrArr[34] 	= mod_hail;
		returnStrArr[35] 	= hvy_hail;
		returnStrArr[36] 	= sunRise;
		returnStrArr[37] 	= sunSet;
		returnStrArr[38] 	= light;
		returnStrArr[39] 	= dark;

		return returnStrArr;
		
	}

}
