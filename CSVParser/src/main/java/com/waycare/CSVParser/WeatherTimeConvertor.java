package com.waycare.CSVParser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

/*
 * Description:
 * 	change weather time format from weather convention to AP convention
 */
public class WeatherTimeConvertor {

	DateTimeFormatter weather_dtf_h = DateTimeFormatter
			.ofPattern("M-d-yyyy H:mm");
	private DateTimeFormatter dtf_h = DateTimeFormatter
			.ofPattern("HH dd/MM/yyyy");

	public void convert(String inputFile, String outputFile) {

		try {
			CSVReader reader = new CSVReader(new FileReader(inputFile));
			CSVWriter writer = new CSVWriter(new FileWriter(outputFile),',');
			
			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {
				String[] strArr = nextLine[0].split(" ");
				
				LocalDateTime weatherDateTime = LocalDateTime.parse(strArr[0] + " " + strArr[1], weather_dtf_h);
				if (weatherDateTime.getMinute() > 50){
					//System.out.println("Added hour");
					weatherDateTime.plusHours(1);
				}
				nextLine[0] = weatherDateTime.format(dtf_h);
				writer.writeNext(nextLine);
				
			}
			
			reader.close();
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
