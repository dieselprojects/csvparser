package com.waycare.CSVParser;

public class WeatherBean {
	
	private int precipitation;
	private boolean wind;
	private boolean fog;
	private boolean rain;
	private boolean snow;
	private boolean surfaceSnow;
	
	public WeatherBean(){
		this.precipitation = 0;
		this.wind = false;
		this.fog = false;
		this.rain = false;
		this.snow = false;
		this.surfaceSnow = false;
	}
	
	public WeatherBean(int precipitation, boolean wind, boolean fog,
			boolean rain, boolean snow, boolean surfaceSnow) {
		super();
		this.precipitation = precipitation;
		this.wind = wind;
		this.fog = fog;
		this.rain = rain;
		this.snow = snow;
		this.surfaceSnow = surfaceSnow;
	}
	
	public int getPrecipitation() {
		return precipitation;
	}
	public void setPrecipitation(int precipitation) {
		this.precipitation = precipitation;
	}
	public boolean isWind() {
		return wind;
	}
	public void setWind(boolean wind) {
		this.wind = wind;
	}
	public boolean isFog() {
		return fog;
	}
	public void setFog(boolean fog) {
		this.fog = fog;
	}
	public boolean isRain() {
		return rain;
	}
	public void setRain(boolean rain) {
		this.rain = rain;
	}
	public boolean isSnow() {
		return snow;
	}
	public void setSnow(boolean snow) {
		this.snow = snow;
	}
	public boolean isSurfaceSnow() {
		return surfaceSnow;
	}
	public void setSurfaceSnow(boolean surfaceSnow) {
		this.surfaceSnow = surfaceSnow;
	}

	
	
}
